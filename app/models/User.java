package models;

import javax.persistence.*;
import play.db.jpa.Model;
import java.util.ArrayList;
import java.util.List;
import java.util.*;
import play.db.jpa.*;
import play.data.validation.*;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;

@Entity
public class User extends Model
{
  @OneToMany(mappedBy="author", cascade=CascadeType.ALL)
  public List<Post> posts;
  @Email
  @Required
  public String email;
  @Required
  public String password;
  
  public String fullname;
  public boolean isAdmin;

  public User(String email, String password, String fullname)
  {
    this.email = email;
    this.password = password;
    this.fullname = fullname;
    this.posts = new ArrayList<Post>();
  }
  
  public static User findByName(String fullname)
  {
	  return find("name", fullname).first();
  }
  
  public static User connect(String email, String password) 
  {
	    return find("byEmailAndPassword", email, password).first();
  }
  
  public void addPost(Post post)
  {
    post.author = this;
    posts.add(post);
  }
  
  public String toString()
  {
    return fullname;
  }
}